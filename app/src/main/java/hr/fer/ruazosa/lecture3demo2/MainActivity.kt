package hr.fer.ruazosa.lecture3demo2

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import hr.fer.ruazosa.lecture3demo2.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        val numberOfClicksViewModel = ViewModelProvider(this,
            ViewModelProvider.AndroidViewModelFactory(application)).get(NumberOfClickViewModel::class.java)

        numberOfClicksViewModel.numberOfClicks.observe(this, Observer {
            binding.numberOfClicksTextViewId.text = "Number of clicks: " + it
        })

        binding.countClickButtonId.setOnClickListener {
            numberOfClicksViewModel.increaseNumberOfClicks()
        }
    }
}