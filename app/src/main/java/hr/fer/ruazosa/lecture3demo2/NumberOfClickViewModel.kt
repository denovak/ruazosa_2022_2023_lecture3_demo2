package hr.fer.ruazosa.lecture3demo2

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class NumberOfClickViewModel: ViewModel() {
    var numberOfClicks = MutableLiveData<Int>()

    init {
        numberOfClicks.value = NumberOfClicksRepository.numberOfClicks
    }
    fun increaseNumberOfClicks() {
        NumberOfClicksRepository.numberOfClicks += 1
        numberOfClicks.value = NumberOfClicksRepository.numberOfClicks
    }
}